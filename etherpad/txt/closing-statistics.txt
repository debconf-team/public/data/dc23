Closing statistics:
    Registration: https://debconf23.debconf.org/register/statistics/
	    Covid Tests taken: ~125
	    Confirmed Covid Cases: 0 (1 turned out to be something else)
    Content: https://debconf23.debconf.org/talks/statistics/
    Network:
        Traffic:
        Primary uplink: 7.3TiB 
        Backup uplink: 360GiB
        Hotel uplink: ?
    Power outage induced reboots: ∞ (until we found the UPS socket in the NOC)
    Broken filesystems: 0 (that we've noticed so far?)
    Access Points Deployed:
        8 + 6 + 9 = 23 (3 different systems)
    VMs deployed on the router:
        5
    Switches Deployed:
        11
    Switches that died:
        1
    Fibres deployed:
        4 (2 ISPs and 1 family networking team)
    Cables out of windows:
        2 CAT5 + 1 fibre
    Cable up staircases:
        3 CAT5 (1 cursed) + 1 fibre
    Possessed talk venues:
        1
    Epileptic fits induced by the stage lighting:
        0 (we hope)
Video:
    Kg of equipment imported:
        75
    CPU encoding time: 
        1.2 days (so far)
    Max remote presenters on stage at once:
        5
    Max concurrent jitsi sessions:
        2
    Number of times the cursed TRS cable was deployed (https://krkr.eu/tmp/2023-09-17-5vIUbZQOEig/cursed-cable.jpg):
        2
    Meters of gaffer tape deployed:
        400
    Number of label tape audits performed (https://krkr.eu/tmp/2023-09-17-5vIUbZQOEig/tape-audit.jpg):
        1
    Streaming Video:
        4.3TiB of streaming traffic
    Hours of video streamed:
        IPv4: 1237.5hr
	  IPv6: 225.7hr
	 Total Hours watched: 1463.2075
       35 countries (geoip)
       186 unique IPs
        Max 28 concurrent stream viewers
Volunteers: https://debconf23.debconf.org/volunteers/statistics/
Remote video review volunteers:
    ivodd, valhalla
Teams: 
    Core Team
    Accessibility
    Design & Artwork
    Bursaries
    Catering
    Cheese & Wine
    Conference Dinner
    Content
    Day Trip
    Sponsors (Fundraising)
    Network Team
    Registration & Front Desk
    Room Assignments
    Swag
    Travel
    Treasurers
    Video
    Visa
    Website
    Debian Community Team
    Debian Publicity Team
    Debian System Administrators
    Debian Legal Team

