Debian Contributors shake-up
===========================
The Debian Contributors Website turns 10 this year, and keeping the data sources that feed it up to date is still an open issue.

This can be a BoF/QA/hack session on the website: https://contributors.debian.org/ ;
corresponding wiki: https://wiki.debian.org/DebianContributors ;
salsa: https://salsa.debian.org/nm-team/contributors.debian.org/-/issues ;
as well as an effort to get teams involved into the data mining task: https://salsa.debian.org/tassia/dc-sources/

	* Short intro on Debian contributors (if needed)
		* acknowledging/thanking contributions
		* the minimum we can do
		* entry point for data protection compliance
		* a way to see more of the breadth of Debian
	* Design choices, and needed changes to make sure the service is funcional
		* simple data model (begin-end dates, no gaps)
		* only dates, no times
		* identities used for contribution (emails, usernames, ...)
		* users who can claim identities
		* team members as data source admins
	* Recently updated sources
	* Missing sources: DebConf, Outreach, Testing, ???

Should "pushing the recorded contribution data to contributors.d.o" become mandatory for hosting a service on debian.org infrastructure? the contributors.d.o team and the data protection team seem in favor of it.

Data privacy issues: contributors.d.o doesn't show data about a person until they claim that they own the identifier associated to the data (so public display of the data is opt-in), except for Debian Developers.- Most of the data collected can already be derived off of public information, indexed by search engines.
