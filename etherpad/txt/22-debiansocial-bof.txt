Debian.Social BoF:

Agenda:
	1. Introduction:
		1.1. Team
		1.2. Current services
	2. Current Services
		2.1. State of the matrix debian packages.
		2.2. Matrix disk space...
		2.3. More SSO
			2.3.1. pleroma?
		2.4. Pleroma -> akkoma?
		2.5. Peertube new physical server is ready, migration after DC23
		2.6. Anything we want to give up on?
			2.6.1. Perhaps plume and writefreely?
	3. Upcoming Services:
		3.7. Discourse?
		3.8. Loomio https://www.loomio.com/
	4. Data Policies?
	5. Growing the team!
		5.1. Anarcat is helping with matrix and other sysadmin tasks
		5.2. Sergio would like to join
		5.3. Q!
	6. New IRC channel for actual social chats?

Notes:
Matrix:
    There are hosted matrix storage available.
    Migration to physical servers could give us a few years' headroom.
SSO migration:
    Make sure accounts aren't lost when we migrate to SSO.
pleroma:
    Apparently accoma is more active than pleroma.
    We could also consider other microblogging alternatives. Mastadon? (possibly better moderation)
    Fediverse now has account migration.
Moderation:
    Haven't had to do much moderation on the fediverse.
    Debian Code of Conduct applies.
    Pleroma has report buttons.
    There are also groups tracking issues within the fediverse space.
    Mjolnir is working well in matrix. Block lists are shared.
    Migrate mjolnir -> draupnir
Wordpress:
    Supports mastadon comments via a plugin, highvoltage is testing it out.
Unused services:
    plume and writefreely (and wordpress) provide the same needs
New services:
    discourse? Python, Gnome, Fedora, Ubuntu have all migrated to discourse to replace lists for discussions.
		It would be good to have an initial user to spearhead discourse use.
		There are places in debian where we could really use reactions (something mailing lists can't do).
	matrix<->telegram bridge. Sounds doable if both sides are well moderated.
	loomio: Decision making - https://www.loomio.com/
	castopod: Podcasts - https://castopod.org/
	CryptPad: Collaborative note-taking - https://cryptpad.fr/
	framasoft has "abc" for decision making.
Data Policies:
    We generally rely on the services to provide the tools we need.
